"""
Author: Nasir Bilal
Email: nbilal@paloaltonetworks.com
"""
from requests.packages import urllib3
import oktapanadmin as okta
import settings.py as settings


##############################################################
# MAIN
##############################################################
def main():

    # Disable SSL Warnings
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    panos_url = settings.panos_url
    panos_user = settings.panos_user
    panos_passwd = settings.panos_passwd
    panos_api_key = okta.keygen(panos_user, panos_passwd, panos_url)
    device = {'template': 'Vantaa'}

    # CREATE AUTHENTICATION PROFILE
    results = okta.create_auth_profile(panos_url, panos_api_key, device['template'])
    print(
        f"CREATING AUTH PROFILE:\n"
        f"CODE: {results['r1'].status_code}\n"
        f"TEXT: {results['r1'].text}"
        )
    print(
        f"CODE: {results['r2'].status_code}\n"
        f"TEXT: {results['r2'].text}\n"
        )
    print(
        f"CODE: {results['r3'].status_code}\n"
        f"TEXT: {results['r3'].text}\n"
        )
    print(
        f"CODE: {results['r4'].status_code}\n"
        f"TEXT: {results['r4'].text}\n"
        )


##############################################################
# RUN IT!
##############################################################
if __name__ == '__main__':
    main()
