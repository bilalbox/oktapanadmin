# oktapanadmin
> Tool for automating configuration of Okta SAML application for PANOS Admin UI of Panorama-managed firewalls via templates.

This script configures the SAML integration between one or more PANOS devices and Okta for the purposes of authentication and authorization of the Admin UI.

## Requirements
- xmltodict
- requests
- xmltodict

## Usage
Create a settings.py file and format it as follows:
```python
okta_api_token = 'YOUR OKTA API TOKEN'
okta_base_url = 'YOUR OKTA BASE URL'
panos_host = 'YOUR PANORAMA MGT IP'
panos_url = f"https://{panos_host}/api"
panos_user = 'YOUR PANORAMA USERNAME'
panos_passwd = 'YOUR PANORAMA PASSWORD'
okta_group_id = 'THE ID OF THE OKTA GROUP TO BE ALLOWED TO LOGIN VIA SAML'
adminrole = 'CORRESPONDING ADMIN ROLE IN PANORAMA'
```

## License
MIT LICENSE
