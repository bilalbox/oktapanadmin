"""
Author: Nasir Bilal
Email: nbilal@paloaltonetworks.com
"""
from requests.packages import urllib3
import oktapanadmin as okta
import settings.py as settings


##############################################################
# MAIN
##############################################################
def main():

    # Disable SSL Warnings
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    api_token = settings.okta_api_token
    base_url = settings.okta_base_url
    app_id = '0oafxqfvo8vBhuZKx0h7'
    hr_app_id = '0oafuqs8hr4DrWZNT0h7'
    group_id = '00gfx6lymjIeUDsQg0h7'
    firewall_ip = '10.104.2.49'
    app_label = "API_TEST_PANOS_Admin"
    app_settings = {
        'name': 'paloaltonetworkssaml',
        'label': f"{app_label}",
        'signOnMode': 'SAML_2_0',
        'settings': {
            'app': {'baseURL': f"https://{firewall_ip}"}
        }
    }
    schema_settings = {
        'definitions': {
            'custom': {
                'id': '#custom',
                'type': 'object',
                'properties': {
                    'adminrole': {
                        'title': 'Admin Role', 'type': 'string', 'scope': 'NONE'
                        }
                    },
                'required': []
            },
            'base': {
                'id': '#base',
                'type': 'object',
                'properties': {
                    'userName': {
                        'title': 'Username',
                        'type': 'string',
                        'required': True,
                        'scope': 'NONE',
                        'maxLength': 100
                    }
                },
                'required': ['userName']
            }
        }
    }

    # apps = okta.get_apps(base_url, api_token, app_id)
    # groups = okta.get_groups(base_url, api_token, app_id)
    # users = okta.get_users(base_url, api_token, app_id)
    # schema_post = okta.update_schema(base_url, api_token, app_id, schema_settings)
    # schemas = okta.get_schema(base_url, api_token, app_id)
    # metadata = okta.get_metadata(base_url, api_token, app_id)
    # print(schema_post)
    # print(schemas)
    # for k, v in schemas.items():
    #     print(f"{k}: {v}")
    # create_app_post = okta.create_app(base_url, api_token, app_settings)
    # print(create_app_post)
    set_group_put = okta.set_group(base_url, api_token, app_id, group_id, 'superuser')
    print(set_group_put)
    print(
        app_settings,
        schema_settings,
        hr_app_id,
    )


##############################################################
# RUN IT!
##############################################################
if __name__ == '__main__':
    main()
