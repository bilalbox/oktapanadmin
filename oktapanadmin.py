"""
Author: Nasir Bilal
Email: nbilal@paloaltonetworks.com
"""
from lxml import etree
import xmltodict
import requests
from requests.packages import urllib3
import json
import settings.py as settings


##############################################################
# OKTA FUNCTIONS
##############################################################
def get_apps(url: str, api_key: str, app_id: str = None) -> dict:
    try:
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': f"SSWS {api_key}",
        }
        if app_id:
            url = f"{url}/apps/{app_id}"
        else:
            url = f"{url}/apps"
        response = requests.get(url, headers=headers, verify=False)
        return json.loads(response.text)

    except BaseException as be:
        return f"get_apps failed due to {be}"


def create_app(url: str, api_key: str, settings: dict) -> str:
    try:
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': f"SSWS {api_key}",
        }
        response = requests.post(f"{url}/apps", json=settings, headers=headers, verify=False)
        return json.loads(response.text)['id']

    except BaseException as be:
        return f"create_app() failed due to {be}"


def get_users(url: str, api_key: str, app_id: str) -> dict:
    try:
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': f"SSWS {api_key}",
        }
        url = f"{url}/apps/{app_id}/users"
        response = requests.get(url, headers=headers, verify=False)
        return json.loads(response.text)

    except BaseException as be:
        return f"get_apps failed due to {be}"


def get_groups(url: str, api_key: str, app_id: str) -> dict:
    try:
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': f"SSWS {api_key}",
        }
        url = f"{url}/apps/{app_id}/groups"
        response = requests.get(url, headers=headers, verify=False)
        return json.loads(response.text)

    except BaseException as be:
        return f"get_apps failed due to {be}"


def set_group(url: str, api_key: str, app_id: str, group_id: str, adminrole: str) -> dict:
    try:
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': f"SSWS {api_key}",
        }
        data = json.dumps({'profile': {'adminrole': f'{adminrole}'}})
        response = requests.put(f"{url}/apps/{app_id}/groups/{group_id}", data=data, headers=headers, verify=False)
        return json.loads(response.text)

    except BaseException as be:
        return f"create_app() failed due to {be}"


def get_metadata(url: str, api_key: str, app_id: str) -> str:
    try:
        headers = {
            'Accept': 'application/xml',
            'Content-Type': 'application/xml',
            'Authorization': f"SSWS {api_key}",
        }
        url = f"{url}/apps/{app_id}/sso/saml/metadata"
        response = requests.get(url, headers=headers, verify=False)
        return response.text

    except BaseException as be:
        return f"get_apps failed due to {be}"


def get_schema(url: str, api_key: str, app_id: str) -> dict:
    try:
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': f"SSWS {api_key}",
        }
        url = f"{url}/meta/schemas/apps/{app_id}/default"
        response = requests.get(url, headers=headers, verify=False)
        return json.loads(response.text)

    except BaseException as be:
        return f"get_apps failed due to {be}"


def update_schema(url: str, api_key: str, app_id: str, settings: dict) -> dict:
    try:
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': f"SSWS {api_key}",
        }
        url = f"{url}/meta/schemas/apps/{app_id}/default"
        data = json.dumps(settings)
        response = requests.post(url, data=data, headers=headers, verify=False)
        return json.loads(response.text)

    except BaseException as be:
        return f"get_apps failed due to {be}"


##############################################################
# PANOS FUNCTIONS
##############################################################
def api_request(url: str, values: dict) -> str:
    try:
        return requests.post(url, values, verify=False).content
    except BaseException as be:
        print(f"api_request() failed due to {be}.")
        return None


def keygen(username: str, password: str, url: str) -> str:
    values = {'type': 'keygen', 'user': username, 'password': password}
    response = api_request(url, values).decode()
    try:
        return etree.fromstring(response).find(".//key").text
    except BaseException as be:
        print(f"keygen() failed due to {be}.")
        return None


def get_devices(url: str, api_key: str) -> list:
    # BUILD DICTIONARY FOR "SHOW DEVICES CONNECTED" AND MAKE API CALL
    api_call_dict = {
        'type': 'op',
        'cmd': '<show><devices><connected/></devices></show>',
        'key': api_key,
    }
    try:
        response = api_request(url, api_call_dict).decode()
        devices = xmltodict.parse(response).get('response', {}).get(
            'result', {}).get('devices', {}).get('entry', {})
        if type(devices) == list:
            return devices
        else:
            return [devices]
    except BaseException as be:
        print(f"get_devices() failed due to {be}.")
        return None


def get_templates(url: str, api_key: str) -> dict:
    # BUILD DICTIONARY FOR "SHOW DEVICES CONNECTED" AND MAKE API CALL
    api_call_dict = {
        'type': 'op',
        'cmd': '<show><template-stack/></show>',
        'key': api_key,
    }
    try:
        response = api_request(url, api_call_dict).decode()
        # print(f"get_templates() response = {response}")
        templates = xmltodict.parse(response).get('response', {}).get(
            'result', {}).get(
                'template-stack', {}).get(
                    'entry', {}
                )
        return templates
    except BaseException as be:
        print(f"get_templates() failed due to {be}.")
        return None


def id_template(url: str, api_key: str, serial: str, templates: list) -> list:
    for t in templates:
        if t.get('devices', {}).get('entry', {}).get('serial', {}) == serial:
            return t.get('templates', {}).get('member', {})


def upload_idpmetadata(url: str, api_key: str, infile: dict, template: str) -> requests.Response:
    api_call_dict_01 = {
        'key': api_key,
        'type': 'import',
        'category': 'idp-metadata',
        'profile-name': f"sidp_{template}",
    }
    try:
        response_01 = requests.post(url, api_call_dict_01, files=infile, verify=False)
        return response_01
    except BaseException as be:
        print(f"upload_idpmetadata() failed due to {be}.")
        return None


def save_config(url: str, api_key: str, filename: str) -> requests.Response:
    try:
        api_call_dict = {
            'key': api_key,
            'type': 'op',
            'cmd': f"<save><config><to>{filename}</to></config></save>"
        }
        response = requests.post(url, api_call_dict, verify=False)
        return response
    except BaseException as be:
        print(f"save_config() failed due to {be}.")
        return None


def move_profile(url: str, api_key: str, template: str, filename: str) -> dict:
    api_call_dict_01 = {
        'key': api_key,
        'type': 'op',
        'cmd': f"<load><config><partial><mode>merge</mode>"
               f"<from>{filename}</from><from-xpath>"
               f"/config/panorama/certificate/"
               f"entry[@name='crt.sidp_{template}.shared']"
               f"</from-xpath><to-xpath>"
               f"/config/devices/entry[@name='localhost.localdomain']"
               f"/template/entry[@name='{template}']/config/shared/"
               f"certificate/entry[@name='crt.sidp_{template}.shared']"
               f"</to-xpath></partial></config></load>",
    }
    api_call_dict_02 = {
        'key': api_key,
        'type': 'op',
        'cmd': f"<load><config><partial><mode>merge</mode>"
               f"<from>{filename}</from><from-xpath>"
               f"/config/panorama/server-profile/saml-idp/"
               f"entry[@name='sidp_{template}']"
               f"</from-xpath><to-xpath>"
               f"/config/devices/entry[@name='localhost.localdomain']"
               f"/template/entry[@name='{template}']/config/shared/"
               f"server-profile/saml-idp/"
               f"entry[@name='sidp_{template}']"
               f"</to-xpath></partial></config></load>",
    }
    # NEED TO MOVE CERTIFICATE BEFORE SAML_IDP TO TARGET TEMPLATE
    try:
        response_01 = requests.post(url, api_call_dict_01, verify=False)
        response_02 = requests.post(url, api_call_dict_02, verify=False)
        return {
            'r1': response_01,
            'r2': response_02,
        }
    except BaseException as be:
        print(f"move_profile() failed due to {be}")
        return None


def create_auth_profile(url: str, api_key: str, template: str) -> dict:
    username = 'username'
    user_group = 'groups'
    access_domain = 'accessdomain'
    admin_role = 'adminrole'

    # Disable IDP Certificate Verification Before Creating Auth Profile
    api_call_dict_01 = {
        'key': api_key,
        'type': 'config',
        'action': 'set',
        'xpath': f"/config/devices/entry[@name='localhost.localdomain']/"
                 f"template/entry[@name='{template}']/config/shared/"
                 f"server-profile/saml-idp/entry[@name='sidp_{template}']",
        'element': f"<validate-idp-certificate>no</validate-idp-certificate>"
                   f"<admin-use-only>yes</admin-use-only>"
    }

    # CREATE PROFILE
    api_call_dict_02 = {
        'key': api_key,
        'type': 'config',
        'action': 'set',
        'xpath': f"/config/devices/entry[@name='localhost.localdomain']/"
                  f"template/entry[@name='{template}']/config/shared/"
                  f"authentication-profile/entry[@name='ap_saml_{template}']",
        'element': f"<method><saml-idp><server-profile>sidp_{template}</server-profile>"
                   f"<attribute-name-username>{username}</attribute-name-username>"
                   f"<attribute-name-usergroup>{user_group}</attribute-name-usergroup>"
                   f"<attribute-name-access-domain>{access_domain}</attribute-name-access-domain>"
                   f"<attribute-name-admin-role>{admin_role}</attribute-name-admin-role>"
                   f"</saml-idp></method>"
    }

    # SET ALLOW LIST TO 'ALL' IN AUTHENTICATION PROFILE
    api_call_dict_03 = {
        'key': api_key,
        'type': 'config',
        'action': 'set',
        'xpath': f"/config/devices/entry[@name='localhost.localdomain']/"
                 f"template/entry[@name='{template}']/config/shared/"
                 f"authentication-profile/entry[@name='ap_saml_{template}']/allow-list",
        'element': f"<member>all</member>"
    }

    # APPLY AUTHENTICATION PROFILE TO DEVICE > SETUP > MANAGEMENT > AUTHENTICATION SETTINGS
    api_call_dict_04 = {
        'key': api_key,
        'type': 'config',
        'action': 'set',
        'xpath': f"/config/devices/entry[@name='localhost.localdomain']/template/"
                 f"entry[@name='{template}']/config/devices/entry[@name='localhost.localdomain']/"
                 f"deviceconfig/system",
        'element': f"<authentication-profile>ap_saml_{template}</authentication-profile>"
    }

    try:
        response_01 = requests.post(url, api_call_dict_01, verify=False)
        response_02 = requests.post(url, api_call_dict_02, verify=False)
        response_03 = requests.post(url, api_call_dict_03, verify=False)
        response_04 = requests.post(url, api_call_dict_04, verify=False)
        return {
            'r1': response_01,
            'r2': response_02,
            'r3': response_03,
            'r4': response_04,
        }
    except BaseException as be:
        print(f"create_auth_profile() failed due to {be}")
        return None


def delete_config(url: str, api_key: str, filename: str) -> requests.Response:
    try:
        api_call_dict = {
            'key': api_key,
            'type': 'op',
            'cmd': f"<delete><config><saved>{filename}</saved></config></delete>"
        }
        response = requests.post(url, api_call_dict, verify=False)
        return response
    except BaseException as be:
        print(f"delete_config() failed due to {be}")
        return None


def clear_idpmetadata(url: str, api_key: str, template: str) -> dict:
    api_call_dict_01 = {
        'key': api_key,
        'type': 'config',
        'action': 'delete',
        'xpath': f"/config/panorama/server-profile/saml-idp/"
                 f"entry[@name='sidp_{template}']",
    }
    api_call_dict_02 = {
        'key': api_key,
        'type': 'config',
        'action': 'delete',
        'xpath': f"/config/panorama/certificate/"
                 f"entry[@name='crt.sidp_{template}.shared']",
    }
    try:
        response_01 = requests.post(url, api_call_dict_01, verify=False)
        response_02 = requests.post(url, api_call_dict_02, verify=False)
        return {
            'r1': response_01,
            'r2': response_02,
        }
    except requests.exceptions.ConnectionError:
        print("ERROR Connecting to {url}.".format(url=url))
        return None


##############################################################
# MAIN
##############################################################
def main():

    # Disable SSL Warnings
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    panos_url = settings.panos_url
    panos_user = settings.panos_user
    panos_passwd = settings.panos_passwd
    panos_api_key = keygen(panos_user, panos_passwd, panos_url)
    okta_api_token = settings.okta_api_token
    okta_base_url = settings.okta_base_url
    okta_group_id = settings.okta_group_id
    adminrole = settings.adminrole

    # get list of devices and templates:
    devices = get_devices(panos_url, panos_api_key)
    templates = get_templates(panos_url, panos_api_key)
    for device in devices:
        device['template'] = id_template(panos_url, panos_api_key, device['serial'], templates)[0]
        app_settings = {
            'name': 'paloaltonetworkssaml',
            'label': f"{device['template']}_Admin_UI_SAML",
            'signOnMode': 'SAML_2_0',
            'settings': {
                'app': {'baseURL': f"https://{device['ip-address']}"}
            }
        }
        schema_settings = {
            'definitions': {
                'custom': {
                    'id': '#custom',
                    'type': 'object',
                    'properties': {
                        'adminrole': {
                            'title': 'Admin Role', 'type': 'string', 'scope': 'NONE'
                            }
                        },
                    'required': []
                },
                'base': {
                    'id': '#base',
                    'type': 'object',
                    'properties': {
                        'userName': {
                            'title': 'Username',
                            'type': 'string',
                            'required': True,
                            'scope': 'NONE',
                            'maxLength': 100
                        }
                    },
                    'required': ['userName']
                }
            }
        }

        print(f"IP: {device['ip-address']}\nTemplate:{device['template']}")
        okta_app_id = create_app(okta_base_url, okta_api_token, app_settings)
        device['metadata'] = get_metadata(okta_base_url, okta_api_token, okta_app_id)
        schema_post = update_schema(okta_base_url, okta_api_token, okta_app_id, schema_settings)
        print(f"SCHEMA UPDATE: {schema_post}")
        set_group_put = set_group(okta_base_url, okta_api_token, okta_app_id, okta_group_id, adminrole)
        print(f"GROUP ASSIGNMENT: {set_group_put}")

        # IMPORT CERTIFICATES AND IDP PROFILES INTO PANORAMA
        infile = {'file': device['metadata']}
        result = upload_idpmetadata(panos_url, panos_api_key, infile, device['template'])
        print(
            f"UPLOADING IDP PROFILE FOR '{device['template']}':\n"
            f"CODE: {result.status_code}\n"
            f"TEXT: {result.text}\n"
        )

    # # SAVING TEMPORARY CONFIG XML TO FACILITATE LOAD CONFIG PARTIAL
    cfg_file = 'panorama_temp_idp.xml'
    result = save_config(panos_url, panos_api_key, cfg_file)
    print(
        f"SAVING TEMP CONFIG FILE '{cfg_file}':\n"
        f"CODE: {result.status_code}\n"
        f"TEXT: {result.text}\n"
        )

    for device in devices:
        # MOVING CERTIFICATES AND IDP PROFILES INTO TEMPLATES
        results = move_profile(panos_url, panos_api_key, device['template'], cfg_file)
        print(
            f"CLONING CERTIFICATE AND IDP PROFILE:\n"
            f"CODE: {results['r1'].status_code}\n"
            f"TEXT: {results['r1'].text}"
            )
        print(
            f"CODE: {results['r2'].status_code}\n"
            f"TEXT: {results['r2'].text}\n"
            )

        # CREATE AUTHENTICATION PROFILE
        results = create_auth_profile(panos_url, panos_api_key, device['template'])
        print(
            f"CREATING AUTH PROFILE:\n"
            f"CODE: {results['r1'].status_code}\n"
            f"TEXT: {results['r1'].text}"
            )
        print(
            f"CODE: {results['r2'].status_code}\n"
            f"TEXT: {results['r2'].text}\n"
            )
        print(
            f"CODE: {results['r3'].status_code}\n"
            f"TEXT: {results['r3'].text}\n"
            )
        print(
            f"CODE: {results['r4'].status_code}\n"
            f"TEXT: {results['r4'].text}\n"
            )

        # DELETING UNNEEDED CERTIFICATE AND IDP PROFILE
        results = clear_idpmetadata(panos_url, panos_api_key, device['template'])
        print(
            f"DELETING UNNEEDED CERTIFICATE AND PROFILE:\n"
            f"CODE: {results['r1'].status_code}\n"
            f"TEXT: {results['r1'].text}"
            )
        print(
            f"CODE: {results['r2'].status_code}\n"
            f"TEXT: {results['r2'].text}\n"
            )

    # DELETING TEMPORARY CONFIG XML
    result = delete_config(panos_url, panos_api_key, cfg_file)
    print(
        f"DELETING TEMP CONFIG FILE '{cfg_file}':\n"
        f"CODE: {result.status_code}\n"
        f"TEXT: {result.text}\n"
        )


##############################################################
# RUN IT!
##############################################################
if __name__ == '__main__':
    main()
